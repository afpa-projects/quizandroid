package main.quizapp

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_question.*
import main.quizapp.dao.DAO
import main.quizapp.models.Answer
import main.quizapp.models.Question

class QuestionActivity : AppCompatActivity(), View.OnClickListener {

    var questionList: ArrayList<Question> = ArrayList()
    var tmpListQuestions: ArrayList<Question> = ArrayList()
    var answerList: ArrayList<Answer> = ArrayList()
    var idThemeRecovered: Int = 0
    var genericDAO = DAO(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)

        intent.extras
        idThemeRecovered = intent.getIntExtra("idTheme", 0)
        questionList = genericDAO.getAllQuestions(idThemeRecovered)
        tmpListQuestions = questionList

        println("liste des questions du theme $idThemeRecovered : $questionList")
        println("la question 0 a l'id : ${questionList[0].id}")

        displayAndSwitchQuestion()
    }

    fun displayAndSwitchQuestion() {

        if (tmpListQuestions.size != 0) {
            textViewQuestion.text = tmpListQuestions[0].toString()

            answerList = genericDAO.getAllAnswers(questionList[0].id)
            println("liste des réponses de la question 0 : $answerList")

            answer1Btn.text = answerList[0].toString()
            answer2Btn.text = answerList[1].toString()
            answer3Btn.text = answerList[2].toString()
            answer4Btn.text = answerList[3].toString()

            clickableButtons(true)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            answer1Btn.id -> {
                correctAnswer(answer1Btn)
                decrementList()
                clickableButtons(false)
                println("la liste après suppression : $tmpListQuestions")
            }
            answer2Btn.id -> {
                decrementList()
                clickableButtons(false)
                println("la liste après suppression : $tmpListQuestions")
            }
            answer3Btn.id -> {
                decrementList()
                clickableButtons(false)
                println("la liste après suppression : $tmpListQuestions")
            }
            answer4Btn.id -> {
                decrementList()
                clickableButtons(false)
                println("la liste après suppression : $tmpListQuestions")
            }
            textViewQuestion.id -> {
                println("le textView marche")
                displayAndSwitchQuestion()
                correctAnswer(textViewQuestion)
            }
        }
    }

    fun correctAnswer(answerText: View) {
        val buttonsList = arrayOf(answer1Btn, answer2Btn, answer3Btn, answer4Btn)
        for (i in 0..3)
            if (buttonsList[i] == answerText && answerList[i].isCorrect) {
                println("réponse correcte")
                buttonsList[i].setBackgroundColor(Color.GREEN)
            } else if (!buttonsList.contains(answerText)) {
                buttonsList[i].setBackgroundResource(android.R.drawable.btn_default)
            } else {
                println("réponse incorrecte")
                buttonsList[i].setBackgroundColor(Color.RED)
            }
    }

    fun clickableButtons(isOk: Boolean) {
        answer1Btn.isClickable = isOk
        answer2Btn.isClickable = isOk
        answer3Btn.isClickable = isOk
        answer4Btn.isClickable = isOk
        textViewQuestion.isClickable != isOk
    }

    fun decrementList() {
        try {
            tmpListQuestions.removeAt(0)
        } catch (e: Exception) {
            println("Empty list : ${e.printStackTrace()}")
        }
    }

}
