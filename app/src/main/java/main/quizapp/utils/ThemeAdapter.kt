package main.quizapp.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import main.quizapp.R
import main.quizapp.models.Theme

class ThemeAdapter(context: Context, resource: Int, theme: ArrayList<Theme>) :
    ArrayAdapter<Theme>(context, resource, theme) {

    private var themes = theme

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var row = convertView

        if (convertView == null) {
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            row = layoutInflater.inflate(R.layout.row_item, parent, false)
            val theme = themes[position]
            val libelle = row.findViewById<TextView>(R.id.titleRow)
            val icon = row.findViewById<ImageView>(R.id.iconRow)

            val iconID = context.resources.getIdentifier(theme.icon, "drawable", context.packageName)
            val drawableIcon = ContextCompat.getDrawable(context, iconID)

            icon.setImageDrawable(drawableIcon)
            libelle.text = theme.name
        }
        return row!!
    }
}