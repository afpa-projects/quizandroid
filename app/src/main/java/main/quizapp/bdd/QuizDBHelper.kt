package main.quizapp.bdd

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import java.io.*

class QuizDBHelper(private val myContext: Context) :
    SQLiteOpenHelper(myContext, DATABASE_NAME, null, DATABASE_VERSION) {

    // chemin défini dans le constructeur

    private val DATABASE_PATH: String

    // Constructeur
    init {
        val filesDir =
            myContext.filesDir.path // /data/data/com.package.nom/files/
        DATABASE_PATH = filesDir.substring(0,
            filesDir.lastIndexOf("/")
        ) + "/databases/"
        // /data/data/com.package.nom/databases/
        // Si la bdd n'existe pas dans le dossier de l'app
        if (!checkDatabase()) { // copy db de 'assets' vers DATABASE_PATH
            copyDatabase()
        }
    }

    private fun checkDatabase(): Boolean {

        // retourne true/false si la bdd existe dans le dossier de l'app

        val dbFile = File(DATABASE_PATH + DATABASE_NAME)
        return dbFile.exists()
    }

    // On copie la base de "assets" vers "/data/data/com.package.nom/databases"
    // ceci est fait au premier lancement de l'application

    private fun copyDatabase() {
        val outFileName = DATABASE_PATH + DATABASE_NAME
        val myInput: InputStream
        try {
            // Ouvre la bdd de 'assets' en lecture
            myInput = myContext.assets.open(DATABASE_NAME)
            // dossier de destination
            val pathFile = File(DATABASE_PATH)
            if (!pathFile.exists()) {
                if (!pathFile.mkdirs()) {
                    Toast.makeText(
                        myContext,
                        "Erreur : copydatabase(), pathFile.mkdirs()",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
            }
            // Ouverture en écriture du fichier bdd de destination
            val myOutput: OutputStream = FileOutputStream(outFileName)
            // transfert de inputfile vers outputfile
            val buffer = ByteArray(1024)
            var length: Int
            while (myInput.read(buffer).also { length = it } > 0) {
                myOutput.write(buffer, 0, length)
            }
            // Fermeture
            myOutput.flush()
            myOutput.close()
            myInput.close()
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(myContext, "Erreur : copydatabase()", Toast.LENGTH_SHORT).show()
        }
        // on greffe le numéro de version
        try {
            val checkDb = SQLiteDatabase.openDatabase(
                DATABASE_PATH + DATABASE_NAME,
                null,
                SQLiteDatabase.OPEN_READWRITE
            )
            checkDb.version =
                DATABASE_VERSION
        } catch (e: SQLiteException) { // bdd n'existe pas
        }
    } // copydatabase()

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(
        db: SQLiteDatabase,
        oldVersion: Int,
        newVersion: Int
    ) {
        if (oldVersion < newVersion) { //Log.d("debug", "onUpgrade() : oldVersion=" + oldVersion + ",newVersion=" + newVersion);
            myContext.deleteDatabase(DATABASE_NAME)
            copyDatabase()
        }
    } // onUpgrade

    companion object {
        private var sInstance: QuizDBHelper? = null

        // l'incrément appelle onUpgrade(), décrément => onDowngrade()
        private const val DATABASE_VERSION =1
        private const val DATABASE_NAME = "sqlite.db"

        @Synchronized
        fun getInstance(context: Context): QuizDBHelper {
            if (sInstance == null) {
                sInstance =
                    QuizDBHelper(context)
            }
            return sInstance as QuizDBHelper
        }
    }


}
