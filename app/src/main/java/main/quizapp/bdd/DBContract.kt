package main.quizapp.bdd

import android.provider.BaseColumns

object DBContract {

    class ThemeEntry : BaseColumns {
        companion object{
            val TABLE_NAME_THEME = "Theme"
            val COLUMN_ID_THEME = "id_theme"
            val COLUMN_TITLE_THEME = "title_theme"
            val COLUMN_ICON_THEME = "icon_theme"
        }
    }

    class QuestionEntry : BaseColumns{
        companion object{
            val TABLE_NAME_QUESTION = "Question"
            val COLUMN_ID_QUESTON = "id_question"
            val COLUMN_TITLE_QUESTION = "title_question"
            val COLUMN_QUESTION_ID_THEME = "id_theme"
        }
    }

    class AnswerEntry:BaseColumns{
        companion object{
            val TABLE_NAME_ANSWER = "Answer"
            val COLUMN_ID_ANSWER = "id_answer"
            val COLUMN_TITLE_ANSWER= "title_answer"
            val COLUMN_CORRECT_ANSWER = "correct_answer"
            val COLUMN_ANSWER_ID_QUESTION = "id_question"
        }
    }

}