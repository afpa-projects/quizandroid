package main.quizapp.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Answer(var id: Int, var title: String, val isCorrect: Boolean) : Parcelable {

    override fun toString(): String {
        return title
    }
}