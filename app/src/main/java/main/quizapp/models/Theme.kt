package main.quizapp.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Theme(var id: Int, var name: String, var icon: String) : Parcelable {

    override fun toString(): String {
        return name
    }
}