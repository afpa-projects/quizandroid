package main.quizapp.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Question(var id: Int, var title: String) : Parcelable {

    override fun toString(): String {
        return title
    }
}