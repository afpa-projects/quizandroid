package main.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import main.quizapp.bdd.QuizDBHelper
import main.quizapp.dao.DAO
import main.quizapp.models.Theme
import main.quizapp.utils.ThemeAdapter

class MainActivity : AppCompatActivity() {

    var themeList: ArrayList<Theme> = ArrayList()

    lateinit var quizDBHelper: QuizDBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar!!.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        quizDBHelper = QuizDBHelper(this)

        var themeDAO = DAO(this)
        themeList = themeDAO.getAllThemes()
        var i = 0

        println("id du premier thème ${themeList[i].id}")
        println("id du deuxième thème ${themeList[1].id} et le nom est ${themeList[1]}")
        println("id du troisième thème ${themeList[2].id}")

        val myAdapter =
            ThemeAdapter(this, R.layout.row_item, themeList)
        listViewThemes.adapter = myAdapter

        listViewThemes.setOnItemClickListener { parent, view, position, id ->
            when (position) {
                0 -> startIntent(position)
                1 -> startIntent(position)
                2 -> startIntent(position)
                3 -> startIntent(position)
            }
        }
    }

    fun startIntent(i: Int) {
        intent = Intent(this, QuestionActivity::class.java)
        intent.putExtra("idTheme", themeList[i].id)
        startActivity(intent)
    }

}




