package main.quizapp.dao

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteException
import main.quizapp.bdd.DBContract
import main.quizapp.bdd.QuizDBHelper
import main.quizapp.models.Answer
import main.quizapp.models.Question
import main.quizapp.models.Theme

class DAO(context: Context) {
    private var maBaseSQLite = QuizDBHelper.getInstance(context)
    private var db = maBaseSQLite.readableDatabase

    fun getAllThemes(): ArrayList<Theme> {
        val themes = ArrayList<Theme>()
        var cursor: Cursor? = null
        try {
            cursor = db?.rawQuery("SELECT * FROM ${DBContract.ThemeEntry.TABLE_NAME_THEME}", null)
        } catch (e: SQLiteException) {
            db?.execSQL(CREATE_TABLE_THEME)
            return ArrayList()
        }

        var themeId: Int
        var title: String
        var icon: String

        while (cursor!!.moveToNext()) {

            themeId = cursor.getInt(cursor.getColumnIndex(DBContract.ThemeEntry.COLUMN_ID_THEME))
            title =
                cursor.getString(cursor.getColumnIndex(DBContract.ThemeEntry.COLUMN_TITLE_THEME))
            if (cursor.getString(cursor.getColumnIndex(DBContract.ThemeEntry.COLUMN_ICON_THEME)) == null)
                icon = ""
            else
                icon =
                    cursor.getString(cursor.getColumnIndex(DBContract.ThemeEntry.COLUMN_ICON_THEME))

            themes.add(Theme(themeId, title, icon))


            println("liste retournée : $themes")

        }

        return themes
    }

    fun getAllQuestions(idTheme: Int): ArrayList<Question> {
        val questions = ArrayList<Question>()
        var cursor: Cursor? = null
        try {
            cursor = db?.rawQuery(
                "SELECT * FROM ${DBContract.QuestionEntry.TABLE_NAME_QUESTION} WHERE ${DBContract.QuestionEntry.COLUMN_QUESTION_ID_THEME} = $idTheme ORDER BY random()",
                null
            )
        } catch (e: SQLiteException) {
            return ArrayList()
        }

        var questionId: Int
        var title: String

        while (cursor!!.moveToNext()) {

            questionId =
                cursor.getInt(cursor.getColumnIndex(DBContract.QuestionEntry.COLUMN_ID_QUESTON))
            title =
                cursor.getString(cursor.getColumnIndex(DBContract.QuestionEntry.COLUMN_TITLE_QUESTION))

            questions.add(Question(questionId, title))
        }

        return questions
    }

    fun getAllAnswers(idQuestion: Int): ArrayList<Answer> {
        val answers = ArrayList<Answer>()
        var cursor: Cursor? = null
        try {
            cursor = db?.rawQuery(
                "SELECT * FROM ${DBContract.AnswerEntry.TABLE_NAME_ANSWER} WHERE ${DBContract.AnswerEntry.COLUMN_ANSWER_ID_QUESTION} = $idQuestion ORDER BY random()",
                null
            )
        } catch (e: SQLiteException) {
            return ArrayList()
        }

        var answerId: Int
        var title: String
        var correctAnswerInt: Int
        var correctAnswerBool: Boolean

        while (cursor!!.moveToNext()) {

            answerId = cursor.getInt(cursor.getColumnIndex(DBContract.AnswerEntry.COLUMN_ID_ANSWER))
            title =
                cursor.getString(cursor.getColumnIndex(DBContract.AnswerEntry.COLUMN_TITLE_ANSWER))
            correctAnswerInt =
                cursor.getInt(cursor.getColumnIndex(DBContract.AnswerEntry.COLUMN_CORRECT_ANSWER))

            if (correctAnswerInt == 1)
                correctAnswerBool = true else correctAnswerBool = false

            answers.add(Answer(answerId, title, correctAnswerBool))

        }

        return answers
    }

    fun open() { //on ouvre la table en lecture/écriture
        db = maBaseSQLite?.getWritableDatabase()
    }

    fun close() { //on ferme l'accès à la BDD
        db!!.close()
    }

    fun addTheme(theme: Theme): Long { // Ajout d'un enregistrement dans la table
        val values = ContentValues()
        values.put(KEY_NAME_THEME, theme.name)
        // insert() retourne l'id du nouvel enregistrement inséré, ou -1 en cas d'erreur
        return db!!.insert(TABLE_NAME_THEME, null, values)
    }

    fun modTheme(theme: Theme): Int { // modification d'un enregistrement
// valeur de retour : (int) nombre de lignes affectées par la requête
        val values = ContentValues()
        values.put(KEY_NAME_THEME, theme.name)
        val where = "$KEY_ID_THEME = ?"
        val whereArgs =
            arrayOf<String>(theme.id.toString() + "")
        return db!!.update(TABLE_NAME_THEME, values, where, whereArgs)
    }

    fun supTheme(theme: Theme): Int { // suppression d'un enregistrement
// valeur de retour : (int) nombre de lignes affectées par la clause WHERE, 0 sinon
        val where = "$KEY_ID_THEME = ?"
        val whereArgs =
            arrayOf<String>(theme.id.toString() + "")
        return db!!.delete(TABLE_NAME_THEME, where, whereArgs)
    }

    fun getTheme(id: Int): Theme { // Retourne l'theme dont l'id est passé en paramètre
        val theme = Theme(0, "", "")
        val cursor: Cursor = db!!.rawQuery(
            "SELECT * FROM ${DBContract.ThemeEntry.TABLE_NAME_THEME} WHERE $KEY_ID_THEME=$id",
            null
        )
        if (cursor.moveToFirst()) {
            theme.id = cursor.getInt(cursor.getColumnIndex(KEY_ID_THEME))
            theme.name = cursor.getString(cursor.getColumnIndex(KEY_NAME_THEME))
            theme.icon = cursor.getString(cursor.getColumnIndex(KEY_ICON_THEME))
            cursor.close()
        }
        return theme
    }

    // sélection de tous les enregistrements de la table


    val themes: Cursor get() = db!!.rawQuery("SELECT * FROM $TABLE_NAME_THEME", null)

    companion object {
        private const val TABLE_NAME_THEME = "Theme"
        const val KEY_ID_THEME = "id_theme"
        const val KEY_NAME_THEME = "title_theme"
        const val KEY_ICON_THEME = "icon_theme"
        const val CREATE_TABLE_THEME = "CREATE TABLE " + TABLE_NAME_THEME +
                " (" +
                " " + KEY_ID_THEME + " INTEGER primary key," +
                " " + KEY_NAME_THEME + " TEXT" +
                ");"
    }
}
